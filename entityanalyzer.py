# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 17:10:36 2021

@author: enovi
"""

import textblob
#Entity Analyzer

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Entity Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):
    
    count = 0
    entitylist = []
    uniques = 0
    blob = textblob.TextBlob(article_text)
    blob.parse()
    blob.tags
    for pair in blob.tags:
        if 'NNP' in pair:
            count += 1

    uniques = len(set(entitylist))        
    
    print('The word count was: ', len(blob.tags))
    print('The total entity count was: ', count)
    print('The unique entity count was: ', uniques)
    if count > 0:
        article_length = len(blob.tags)
        print('Entity density was: ', count / article_length)
        print('Unique entity density was: ', uniques / article_length)
    elif count == 0:
        print('No entities were detected.')
        
main()
